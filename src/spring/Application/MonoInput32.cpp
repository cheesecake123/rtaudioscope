#include "spring/Application/MonoInput32.h"
#include <qendian.h>
#include <iostream>

MonoInput32::MonoInput32(double timeSlice, unsigned sampleRate)
	:dataLength(timeSlice * sampleRate),
	channelBytes(4)
{
	audioFormat.setSampleRate(sampleRate);
	audioFormat.setChannelCount(1);
	audioFormat.setSampleSize(32);
	audioFormat.setCodec("audio/pcm");
	audioFormat.setByteOrder(QAudioFormat::LittleEndian);
	audioFormat.setSampleType(QAudioFormat::SignedInt);

	maxAmplitude = INT32_MAX;
}


MonoInput32::~MonoInput32()
{
}

qint64 MonoInput32::writeData(const char *data, qint64 len)
{
	//const auto *ptr = reinterpret_cast<const unsigned char *>(data);
	const auto *ptr = data;

	for (auto i = 0; i < len / channelBytes; ++i) {

		qint32 value = 0;

		value = qFromLittleEndian<qint32>(ptr);
		
		double level = float(value) * (5./ maxAmplitude);
		timeData.push_back(level);
		ptr += channelBytes;
	}

	if (timeData.size() > dataLength)
		timeData.remove(0, timeData.size() - dataLength);

	return len;
}


QAudioFormat MonoInput32::getAudioFormat()
{
	return audioFormat;
}

QVector<double> MonoInput32::vecGetData() const
{
	return timeData;
}